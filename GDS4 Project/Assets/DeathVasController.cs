﻿using UnityEngine;
using System.Collections;

public class DeathVasController : MonoBehaviour {

	// Use this for initialization
	void Start () {
			GameState.instance.deathVasObject = this.gameObject;
						
			CanvasRenderer theRenderer = (CanvasRenderer)gameObject.GetComponentInChildren<CanvasRenderer>();
			
			theRenderer.SetAlpha(0.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
