﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	bool iscredits = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if(!iscredits)
		{
			gameObject.transform.position = new Vector3(-1.23f,-0.35f,0.0f);
		}
		else
		{
			gameObject.transform.position = new Vector3(-1.23f,-1.21f,0.0f);
		}
		
		if(Input.GetKeyDown(KeyCode.UpArrow) && iscredits)
		{
			iscredits = false;
		}
		
		if(Input.GetKeyDown(KeyCode.DownArrow) && !iscredits)
		{
			iscredits = true;
		}
		
		if(Input.GetKeyUp(KeyCode.LeftShift))
		{
			if(iscredits)
			{
				Application.LoadLevel("CreditsScene");
			}
			else
			{
				Application.LoadLevel("LoadingScene");
			}
		}
	}
}
