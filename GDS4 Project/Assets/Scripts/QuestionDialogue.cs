﻿/***************************************************************
  Script author: Alexander Ong, s3487787
  Beta release: 04/05/15
  
  Free to use and edit, however please do not remove this 
    reference tag
 ***************************************************************/

using UnityEngine;
using System.Collections;

public class QuestionDialogue : MonoBehaviour {
	
	/*******************************************************************************
	 * EDITABLE
	 * ****************************************************************************/
	// Add more text file variables or even plain strings if you want more options for different text
	public string displaystring;
	
	// This is what controls which button cycles through the text as defined in Edit > Project Settings > Input
	string nextTextButton = "Jump";
	/*******************************************************************************
	 * END EDITABLE
	 * ****************************************************************************/
	
	public float textRate = 20;
	public float textRateSpeedup = 2;
	public float textBoxWidthSizePercent = 95f;
	public float textBoxHeightSizePercent = 25f;
	public GUIStyle text_box;
	private QuestionMessage questionMessage;
	private int activeQuestion = 0;
	private bool candismiss = false;
	
	GUIStyle questionFont;
	
	float textBoxSize_x;
	float textBoxSize_y;
	float textBoxPos_x;
	float textBoxPos_y;
	
	string shownText;
	string hiddenText;
	
	int linesPerScreen;
	
	float lineSize;
	float boxHeight;
	
	bool display_text = false;
	bool isRunning = false;
	
	void Start () {
	
		questionFont = new GUIStyle();
		questionFont.font = text_box.font;
		questionFont.fontSize = (int)((text_box.fontSize * (Screen.height / 350.0)));// * (Screen.height / Screen.width));
		
		init ();
		
		/*******************************************************************************
		 * EDITABLE
		 * ****************************************************************************/
		// Set a seperate string variable to each file variable
		
		// Pre-word wrap each dialogue instance
		// eg: [string_variable] = word_wrap ([string_variable], lineSize, text_box);
		//text1 = word_wrap (text1, lineSize, text_box);

        
		/*******************************************************************************
		 * END EDITABLE
		 * ****************************************************************************/
	}
	
	/*******************************************************************************
	 * EDITABLE
	 * ****************************************************************************/
	// Trigger for text, best to use an "OnTriggerEnter" function instead of an "Update" function
	// for practicallity. "Update" is just being used for testing purposes

    public void beginMessage(QuestionMessage theMessage)
    {
        displayText(theMessage);	
    }

	/*******************************************************************************
	 * END EDITABLE
	 * ****************************************************************************/
	
	void OnGUI () {
		if (display_text) {
		
			//Question message. 
			GUI.Label (new Rect (textBoxPos_x, textBoxPos_y, textBoxSize_x, textBoxSize_y), shownText, text_box);
			
			for(int i = 0;i<questionMessage.questions.Count; i++)
			{
				if(activeQuestion == i)
				{
					questionFont.normal.textColor = Color.red;
				}
				else
				{
					questionFont.normal.textColor = Color.white;
				}
				GUI.Label (new Rect (textBoxPos_x+50, textBoxPos_y+60+(30*i), textBoxSize_x, textBoxSize_y), questionMessage.questions[i].question,questionFont);
			}
		}
	}
	
	void init() {
		// Calculate dialogue textbox size
		textBoxSize_x = Screen.width * (textBoxWidthSizePercent / 100);
		textBoxSize_y = Screen.height * (textBoxHeightSizePercent / 100);
		
		// Calculate dialogue textbox position
		textBoxPos_x = Screen.width / 2 - (textBoxSize_x / 2);
		textBoxPos_y = Screen.height - (textBoxSize_y + 10);
		
		// Scale text font size
		text_box.fontSize = (int)((text_box.fontSize * (Screen.height / 350.0)));// * (Screen.height / Screen.width));
		
		// Scale padding to accomodate for resolution
		text_box.padding.left = (int) ((float) text_box.padding.left * (textBoxSize_x / 1000));
		text_box.padding.right = (int) ((float) text_box.padding.right * (textBoxSize_x / 1000));
		text_box.padding.top = (int) ((float) text_box.padding.top * (textBoxSize_y/ 1000));
		text_box.padding.bottom = (int) ((float) text_box.padding.bottom * (textBoxSize_y/ 1000));
		
		lineSize = textBoxSize_x - (text_box.padding.left + text_box.padding.right);	// Take padding into account
		boxHeight = textBoxSize_y - (text_box.padding.top + text_box.padding.bottom);
		
		linesPerScreen = (int)((boxHeight / text_box.lineHeight));
	}
	
	void displayText (QuestionMessage theQuestionMessage)
	{
		if (isRunning == false)
		 {		
		
			questionMessage = theQuestionMessage;
			display_text = true;		// Turn on "OnGUI" function
			
			shownText = "\n   "+theQuestionMessage.questionMessage;
			
		}
	}
	
	void Update() {
		if(display_text)
		{
			//Dirty fix for a big problem.
			if(Input.GetKeyUp(KeyCode.LeftShift))
			{
				candismiss = true;
			}
			
			if(Input.GetKeyDown(KeyCode.UpArrow))
			{
				if(activeQuestion != 0)
				{
					--activeQuestion;
				}
			}
			
			if(Input.GetKeyDown(KeyCode.DownArrow))
			{
				if( activeQuestion != (questionMessage.questions.Count-1))
				{
					++activeQuestion;
				}
			}
			
			if(Input.GetKeyDown(KeyCode.LeftShift))
			{
				if(candismiss)
				{
					//Select the currently active question. 
					display_text = false;
					
					MessageDelegate.instance.questionFinishedShowing(questionMessage.questions[activeQuestion],questionMessage);
				}
			}
		}
	}
	

	
}
