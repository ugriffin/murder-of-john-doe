﻿using UnityEngine;
using System.Collections;

public abstract class TargetableObject : MonoBehaviour {


	//Inheritable type for targetable objects. 

	public abstract void targetObjectClicked();

}
