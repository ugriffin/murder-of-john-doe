﻿/***************************************************************
  Script author: Alexander Ong, s3487787
  Beta release: 04/05/15
  
  Free to use and edit, however please do not remove this 
    reference tag
 ***************************************************************/

using UnityEngine;
using System.Collections;

public class Dialogue : MonoBehaviour {
	
	/*******************************************************************************
	 * EDITABLE
	 * ****************************************************************************/
	// Add more text file variables or even plain strings if you want more options for different text
	public string displaystring;
	
	// Create a string variable for each file variable you have
	string text1;
	
	// This is what controls which button cycles through the text as defined in Edit > Project Settings > Input
	string nextTextButton = "Jump";
	/*******************************************************************************
	 * END EDITABLE
	 * ****************************************************************************/
	
	public float textRate = 20;
	public float textRateSpeedup = 2;
	public float textBoxWidthSizePercent = 95f;
	public float textBoxHeightSizePercent = 25f;
	public GUIStyle text_box;
	
	float textBoxSize_x;
	float textBoxSize_y;
	float textBoxPos_x;
	float textBoxPos_y;
	
	string shownText;
	string hiddenText;
	
	int linesPerScreen;
	
	float lineSize;
	float boxHeight;
	
	bool display_text = false;
	bool isRunning = false;
	
	void Start () {
		init ();
		
		/*******************************************************************************
		 * EDITABLE
		 * ****************************************************************************/
		// Set a seperate string variable to each file variable
        text1 = "null";//displaystring;
		
		// Pre-word wrap each dialogue instance
		// eg: [string_variable] = word_wrap ([string_variable], lineSize, text_box);
		text1 = word_wrap (text1, lineSize, text_box);

        
		/*******************************************************************************
		 * END EDITABLE
		 * ****************************************************************************/
	}
	
	/*******************************************************************************
	 * EDITABLE
	 * ****************************************************************************/
	// Trigger for text, best to use an "OnTriggerEnter" function instead of an "Update" function
	// for practicallity. "Update" is just being used for testing purposes

    public void beginMessage(string theMessage)
    {
        displayText(theMessage);	
    }

	/*******************************************************************************
	 * END EDITABLE
	 * ****************************************************************************/
	
	void OnGUI () {
		if (display_text) {
			GUI.Label (new Rect (textBoxPos_x, textBoxPos_y, textBoxSize_x, textBoxSize_y), shownText, text_box);
		}
	}
	
	void init() {
		// Calculate dialogue textbox size
		textBoxSize_x = Screen.width * (textBoxWidthSizePercent / 100);
		textBoxSize_y = Screen.height * (textBoxHeightSizePercent / 100);
		
		// Calculate dialogue textbox position
		textBoxPos_x = Screen.width / 2 - (textBoxSize_x / 2);
		textBoxPos_y = Screen.height - (textBoxSize_y + 10);
		
		// Scale text font size
		text_box.fontSize = (int)((text_box.fontSize * (Screen.height / 350.0)));// * (Screen.height / Screen.width));
		
		// Scale padding to accomodate for resolution
		text_box.padding.left = (int) ((float) text_box.padding.left * (textBoxSize_x / 1000));
		text_box.padding.right = (int) ((float) text_box.padding.right * (textBoxSize_x / 1000));
		text_box.padding.top = (int) ((float) text_box.padding.top * (textBoxSize_y/ 1000));
		text_box.padding.bottom = (int) ((float) text_box.padding.bottom * (textBoxSize_y/ 1000));
		
		lineSize = textBoxSize_x - (text_box.padding.left + text_box.padding.right);	// Take padding into account
		boxHeight = textBoxSize_y - (text_box.padding.top + text_box.padding.bottom);
		
		linesPerScreen = (int)((boxHeight / text_box.lineHeight));
	}
	
	void displayText (string passage) {
		if (isRunning == false) {		// Prevents coroutine being run multiple times. 
			display_text = true;		// Turn on "OnGUI" function
			StartCoroutine (animateText(passage));
		}
	}
	
	IEnumerator animateText (string passage) {
		float charCount = 0;
		float originalTextRate = textRate;
		int startDisplay = 0;
		bool endOfBox = false;
		
		isRunning = true;
		
		if (textRate <= 0) {
			shownText = passage;
		} else {
			float defaultTextRate = textRate;
			while ((int)charCount <= passage.Length) {
				bool skip = false;
				// No need to change text if already at the end
				if (endOfBox == false) {
					shownText = passage.Substring (startDisplay, (int)charCount - startDisplay);
					
					// Check for last \n
					char[] text_char_array = shownText.ToCharArray();
					int lastBreak = -1;
					
					for (int i = 0; i < text_char_array.Length; i++) {
						if (text_char_array[i] == '\n') {
							lastBreak = i;
							
							// Check if a new paragraph
							if (i != 0) {
								if (text_char_array[i - 1] == '\n') {
									skip = true;
									lastBreak = i;
								}
							}
						}
					}
					
					if (lineCount(shownText) < linesPerScreen && skip == false) {
						// Correct for it increasing by more than 1
						charCount += 1 * Time.deltaTime * textRate;
					} else {												// Start a new paragraph
						if (lastBreak != -1) {
							charCount = startDisplay + lastBreak + 1;		// include startDisplay as lastBreak will not take into account previous text
							shownText = passage.Substring (startDisplay, (int)charCount - startDisplay);
						}
						endOfBox = true;		// End of textbox, stop
					}
					
					// Control textRate speed
					if (Input.GetKeyDown(KeyCode.LeftShift) && (int)charCount != 0) { // Trigger for text && Delay speedup for one frame
						textRate *= textRateSpeedup;
					} else if (Input.GetButtonUp (nextTextButton)) {
						textRate = originalTextRate;
					}
				}
				
				if (Input.GetKeyDown(KeyCode.LeftShift) && endOfBox == true) {		// Control to move to next
					//charCount++;
					startDisplay = (int)charCount;
					//lineCount = 0;
					endOfBox = false;
				}
				
				yield return null;
			}
			shownText = passage.Substring (startDisplay, passage.Length - startDisplay);	// End of string, display final text (gets cut off otherwise)
			
			textRate = defaultTextRate;			// Reset textRate back to original
			
			// Control to exit dialogue box
			bool closeDialogue = false;
			while (!closeDialogue) {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    closeDialogue = true;
                    MessageDelegate.instance.messageFinishedShowing();
                }
				yield return null;
			}
		}
		isRunning = false;
		display_text = false;
	}
	
	// Inserts line breaks (\n) to wrap text
	string word_wrap (string unwrapped, float line_width, GUIStyle font) {	// max_chars is max per line
		char[] dialogue;
		string wrapped;
		
		// Position variables to iterate through char array
		int startPos = 0;		// Position of first char in line
		int lastSpace = -1;
		
		// Make string iteratable  to find spaces
		dialogue = unwrapped.ToCharArray();
		
		// Loop till end of char array
		for (int currentPos = 0; currentPos < dialogue.Length; currentPos++) {
			string line;
			float sizeOftext;
			
			// Was previously "if" instead of "while" here, self note in case things go wrong. >_>
			while (dialogue [currentPos].CompareTo ('\n') == 0) {		// Check for previous breaks
				// remove more than 2 "\n"s
				if (currentPos > 1 && 
				    dialogue [currentPos - 1].CompareTo ('\n') == 0 &&
				    dialogue [currentPos - 2].CompareTo ('\n') == 0) {
					deleteChar(ref dialogue, currentPos);
				} else {
					// Reset to next chars
					currentPos += 1;
					startPos = currentPos;
				}
			}
			
			// Measure pixel size of text
			line = new string(dialogue, startPos, currentPos - startPos + 1); 	// +1 to get string up to the current position. 
			sizeOftext = font.CalcSize(new GUIContent(line)).x; 
			
			if (sizeOftext > line_width) {		// Run out of space on line
				// Shift text to next line
				if (lastSpace != -1) {		// Check if there has been a previous space
					
					// Add break at previous space
					dialogue[lastSpace] = '\n';
					
					// Reset to next chars
					startPos = lastSpace + 1;
					currentPos = startPos;
					lastSpace = -1;			// Set to no previous space
					
				} else {		// Whole line taken up by one word
					Debug.LogError("Word length takes up whole line. " +
					               "Try reducing the font size or shift a portion of the word onto a new line.");
				}
			} else if (dialogue [currentPos].CompareTo (' ') == 0) {	// Check for spaces
				lastSpace = currentPos;
			}
		}
		
		wrapped = new string (dialogue);
		return wrapped;
	}
	
	int lineCount (string text) {
		char[] text_char = text.ToCharArray ();
		int numOfLines = 0;
		
		for (int i = 0; i < text_char.Length; i++) {
			if (text_char[i] == '\n') {
				numOfLines++;
			}
		}
		
		return numOfLines;
	}
	
	void deleteChar (ref char[] str, int pos) {
		ArrayList list = new ArrayList(str);
		list.RemoveAt (pos);
		str = list.ToArray (typeof(char)) as char[];
	}
	
}
