﻿//GameState singleton

using UnityEngine;
using System.Collections;

public sealed class GameState
{
	
	public GameObject mainCharacter = null;
	public GameObject canvasObject = null;
	public GameObject deathVasObject = null;

	public bool isTouchingLadder = false;
	
	public bool foundNewsPaper = false;
	
	/*GameState:
	0: Game Start
	1: Photo of Dead Body taken
	2: Talked to James after photo of dead body taken
	*/
	
	public int theGameState = 0;
	public bool canTalkToHayman = false;
	
	private int deathhits = 0;
		
	private static GameState _instance = null;
	
	//Constructor
	private GameState()
	{
		//Load the control scheme from the SaveDelegate. 
	}
	
	//Singleton instantiator function.
	public static GameState instance
	{
		get
		{
			if (_instance==null)
			{
				_instance = new GameState();
			}
			return _instance;
		}
	}
	
	//Dummy initialisation method for game startup.
	public void init()
	{
		
	}
	
	public void addDeathState(int deathpoints)
	{
		
		if(deathhits == 0)
		{
			MessageDelegate.instance.showMessage("You just asked a dangerous question. Dangerous questions draw the killer closer to you.");
			MessageDelegate.instance.showMessage("Some questions are particularly dangerous and will draw the killer more quickly.");
			MessageDelegate.instance.showMessage("Be careful. If the killer gets too close, you're dead.");
		}
		
		deathhits+= deathpoints;
		
		float thealpha = 0.0f;
		
		switch(deathhits)
		{
			case 1:
			{
				thealpha = 0.2f;
				AudioDelegate.instance.playHBSlow();
				break;
			}
			case 2:
			{
				thealpha = 0.3f;
				AudioDelegate.instance.playHBMedium();
				break;
			}
			case 3:
			{
				thealpha = 0.5f;
				AudioDelegate.instance.playHBFast();
				break;
			}
		}
		
		if(deathhits>3)
		{
			this.doGameOver(1);
		}
		
		CanvasRenderer theRenderer = (CanvasRenderer)deathVasObject.GetComponentInChildren<CanvasRenderer>();
		
		theRenderer.SetAlpha(thealpha);
		
	}
	
	public void doGameOver(int type)
	{
		Application.LoadLevel("GameOver");
		MessageDelegate.instance.showMessage("TestGameOver");
	}
}

