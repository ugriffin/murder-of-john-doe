﻿using UnityEngine;
using System.Collections;

public class GraffitiController : TargetableObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override void targetObjectClicked()
	{
		if(GameState.instance.theGameState == 2)
		{
			if (MessageDelegate.instance.isMessageDisplaying() == false) {
				MessageDelegate.instance.showMessage("You have taken a photograph of the graffiti.");
				MessageDelegate.instance.showMessage("It reads: SINGING IS BLISS, INSIDE THE BOX with writing below that says: Where do you go? Find me in the back.");
				MessageDelegate.instance.showMessage("You: I wonder what that is? I think I need to look around for more things.");
				GameState.instance.theGameState = 3;
			}
		}
		
	}
}
