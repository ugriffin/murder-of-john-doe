﻿using UnityEngine;
using System.Collections;

public class AudioDelegate {

	private static AudioDelegate _instance = null;
	
	private AudioSource bgmAudioSource;
	private AudioSource effectsAudioSource;
	private AudioSource heartBeatAudioSource;
	
	private AudioClip bgmMusic;
	
	private AudioClip huh1;
	private AudioClip huh2;
	private AudioClip hmm;
	
	private AudioClip heartbeatslow;
	private AudioClip heartbeatsmedium;
	private AudioClip heartbeatsfast;
	
	private GameObject audioSource = null;

	//Constructor
	private AudioDelegate()
	{
		audioSource = new GameObject("AudioSourceDelegate");
		audioSource.AddComponent<AudioSource>();
		audioSource.AddComponent<AudioSource>();
		audioSource.AddComponent<AudioSource>();
		
		GameObject.DontDestroyOnLoad(audioSource);
		
		
		this.bgmAudioSource = audioSource.GetComponents<AudioSource>()[0];
		this.effectsAudioSource = audioSource.GetComponents<AudioSource>()[1];
		this.heartBeatAudioSource = audioSource.GetComponents<AudioSource>()[2];
		
		heartBeatAudioSource.loop = true;
		
		
		
		bgmMusic = AudioClip.Instantiate(Resources.Load("BGM")) as AudioClip;
		huh1 = AudioClip.Instantiate(Resources.Load("huh1")) as AudioClip;
		huh2 = AudioClip.Instantiate(Resources.Load("huh2")) as AudioClip;
		hmm = AudioClip.Instantiate(Resources.Load("hmm1")) as AudioClip;
		heartbeatslow = AudioClip.Instantiate(Resources.Load("heartbeatsmall")) as AudioClip;
		heartbeatsmedium = AudioClip.Instantiate(Resources.Load("heartbeatmedium")) as AudioClip;
		heartbeatsfast = AudioClip.Instantiate(Resources.Load("heartbeatfast")) as AudioClip;
		
		GameObject.DontDestroyOnLoad(bgmMusic);
		GameObject.DontDestroyOnLoad(huh1);
		GameObject.DontDestroyOnLoad(huh2);
		GameObject.DontDestroyOnLoad(hmm);
		GameObject.DontDestroyOnLoad(heartbeatslow);		
		GameObject.DontDestroyOnLoad(heartbeatsmedium);		
		GameObject.DontDestroyOnLoad(heartbeatsfast);
		
		
		bgmAudioSource.clip = bgmMusic;
		}
	
	//Singleton instantiator function.
	public static AudioDelegate instance
	{
		get
		{
			if (_instance==null)
			{
				_instance = new AudioDelegate();
			}
			return _instance;
		}
	}
	
	public void playMusic()
	{
		bgmAudioSource.loop = true;
		
		bgmAudioSource.Play();
	}
	
	public void playHuh1()
	{
		effectsAudioSource.clip = huh1;
		
		effectsAudioSource.Play();
	}
	
	public void playHuh2()
	{
		effectsAudioSource.clip = huh2;
		
		effectsAudioSource.Play();
	}
	
	public void playHmm()
	{
		effectsAudioSource.clip = hmm;
		
		effectsAudioSource.Play();
	}
	
	public void playHBSlow()
	{
		heartBeatAudioSource.clip = heartbeatslow;
		
		heartBeatAudioSource.Play();
	}
	
	public void playHBMedium()
	{
		heartBeatAudioSource.clip = heartbeatsmedium;
		
		heartBeatAudioSource.Play();
	}
	
	public void playHBFast()
	{
		heartBeatAudioSource.clip = heartbeatsfast;
		
		heartBeatAudioSource.Play();
	}

}
