﻿//MessageDelegate singleton

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void Callback(); 

public enum MessageEffect {
	NoEffect,
	Huh1,
	Huh2,
	Hmm
}

public class Question
{
	public List<Message> questionAnswers;
	int questionAnswerIndex = 0;
	public string question = "<<NO QUESTION DEFINED>>";
	public int dangerRating = 0; //0 to 2.
	public bool endsDialogue = false;
	public Callback callBack = null;
	
	//Shows after all the questionAnswers are shown. 
	public QuestionMessage linkedQuestionMessage = null;
	
	
	public Question()
	{
		questionAnswers = new List<Message>();
	}
	
	public void addAnswer(string theAnswer,MessageEffect theEffect)
	{
			Message aNewMessage = new Message();
		
			aNewMessage.message = theAnswer;
			aNewMessage.effect = theEffect;	
			questionAnswers.Add(aNewMessage);
	}
	
	public void addAnswer(string theAnswer)
	{
		this.addAnswer(theAnswer,MessageEffect.NoEffect);
	}
}

public class QuestionMessage
{
	public List<Question> questions;
	public string questionMessage;

	public QuestionMessage()
	{
		questions = new List<Question>();
	}
	
	public void addQuestion (Question theQuestion)
	{
		questions.Add(theQuestion);
	}	
}

public class Message
{
	//If this is null, then it's a normal-type message. 
	public QuestionMessage questionmessage = null;
	public string message = "Hello World!";
	public MessageEffect effect = MessageEffect.NoEffect;
}

public sealed class MessageDelegate
{
	private static MessageDelegate _instance = null;
	private List<Message> messages;
    private int messageIndex = 0;

    private GameObject messageObject = null;
    private bool messageIsDisplaying = false;

	//Constructor
	private MessageDelegate()
	{
        messages = new List<Message>();
	}
	
	//Singleton instantiator function.
	public static MessageDelegate instance
	{
		get
		{
			if (_instance==null)
			{
				_instance = new MessageDelegate();
			}
			return _instance;
		}
	}
	
	//Dummy initialisation method for game startup.
	public void init()
	{
		
	}
	
	public void showMessage(string theMessage,MessageEffect theEffect)
	{
		Message aMessage = new Message();
		aMessage.message = theMessage;
		aMessage.effect = theEffect;
		
        messages.Add(aMessage);

        if (!messageIsDisplaying)
        {
            messageIsDisplaying = true;
            
            if(GameState.instance.canvasObject != null)
            {
            	GameState.instance.canvasObject.SetActive(false);
            }
            
            this.displayMessage();
        }
	}
	
	public void showMessage(string theMessage)
	{
		this.showMessage(theMessage,MessageEffect.NoEffect);
	}
	
	public void showQuestionMessage(QuestionMessage theQuestionMessage,MessageEffect theMessageEffect)
	{
		//Adds a question message to the queue. 
		Message aQuestionMesssage = new Message();
		aQuestionMesssage.questionmessage = theQuestionMessage;
		aQuestionMesssage.effect = theMessageEffect;
		
		messages.Add(aQuestionMesssage);
		
		if (!messageIsDisplaying)
		{
			messageIsDisplaying = true;
			
			if(GameState.instance.canvasObject != null)
			{
				GameState.instance.canvasObject.SetActive(false);
			}
			
			this.displayMessage();
		}
	}
	
	public void showQuestionMessage(QuestionMessage theQuestionMessage)
	{
		this.showQuestionMessage(theQuestionMessage,MessageEffect.NoEffect);
	}

    private void displayMessage()
    {
        //Displays a message by generating a displayMessagePrefab. 
        if (messageIndex < messages.Count)
        {
        	if(messages[messageIndex].questionmessage == null)
        	{
	            Message aMessage = messages[messageIndex];
	            
				string theMessage = aMessage.message;
				MessageEffect theEffect = aMessage.effect;
				
	            ++messageIndex; //Add index by one. 
	
	            //Instantiate the prefab to show messages here...
	
	            messageObject = GameObject.Instantiate(Resources.Load("objMessageShower")) as GameObject;
	
	            Dialogue theDialogueObject = messageObject.GetComponent<Dialogue>();
	            
	            switch(theEffect)
	            {
	            	case MessageEffect.Hmm:
	            	{
	            		AudioDelegate.instance.playHmm();
	            		break;
	            	}
	            	
	            	case MessageEffect.Huh1:
	            	{
	            		AudioDelegate.instance.playHuh1();
	            		break;
	            	}
	            	
	            	case MessageEffect.Huh2:
	            	{
	            		AudioDelegate.instance.playHuh2();
	            		break;
	            	}
	            	
	            	default:
	            	
	            	{
	            		break;
	            	}
	            }
	
	            theDialogueObject.beginMessage(theMessage);
            }
            else
            {
            	//It's a question. 
            	Message theMessage = messages[messageIndex];
				QuestionMessage theQuestionMessage = theMessage.questionmessage;
				MessageEffect theEffect = theMessage.effect;
				
				switch(theEffect)
				{
				case MessageEffect.Hmm:
				{
					AudioDelegate.instance.playHmm();
					break;
				}
					
				case MessageEffect.Huh1:
				{
					AudioDelegate.instance.playHuh1();
					break;
				}
					
				case MessageEffect.Huh2:
				{
					AudioDelegate.instance.playHuh2();
					break;
				}
					
				default:
					
				{
					break;
				}
				}				
				//Otherwise the question repeats infinitely. 
				messages.RemoveRange(0,messageIndex+1);
				
				//Reset messageIndex to 0, as we're basically restarting and reflushing the whole thing. 
				messageIndex = 0;
				
				//Instantiate the prefab to show messages here...
				
				
				
				messageObject = GameObject.Instantiate(Resources.Load("objQuestionShower")) as GameObject;
				
				QuestionDialogue theDialogueObject = messageObject.GetComponent<QuestionDialogue>();
				
				theDialogueObject.beginMessage(theQuestionMessage);
			}
			
		}
		else
		{
			messages.Clear();
            messageIndex = 0;
            messageIsDisplaying = false;
            messageObject = null;
            
			if(GameState.instance.canvasObject != null)
			{
				GameState.instance.canvasObject.SetActive(true);
			}
		}
		
	}
	
	//A callback function which allows the MessageDelegate to display the next message in the queue. 
    public void messageFinishedShowing()
    {
        //Wipe the message showing prefab
        GameObject.Destroy(messageObject);

        //If there's no more messages to be displayed, displayMessage wipes memory and resets things. 
        this.displayMessage();
    }
    
    //Called by the Question prefab monobehaviour. Passes info about the question asked, plus the parent question message container if necessary. 
    public void questionFinishedShowing(Question theRepliedQuestion,QuestionMessage theParentQuestionMessage)
    {
    	//First, kill the messageobject...
		GameObject.Destroy(messageObject);
		
		//Get the callback to the message, if there is one...
		Callback theCallback = theRepliedQuestion.callBack;
		
		if(theCallback != null)
		{
			//Run the callback.
			theCallback();
		}
		
		//If this question ends the dialogue...
		if(theRepliedQuestion.endsDialogue)
		{
			//Insert the final dialogue, in reverse order, to the messagequeue (because there may be messages in there). 
			for(int i = theRepliedQuestion.questionAnswers.Count-1; i>-1; i--)
			{
				Message aMessage = theRepliedQuestion.questionAnswers[i];
				messages.Insert(0, aMessage);
			}
			
			this.displayMessage();
		}
		else
		{
			if(theRepliedQuestion.linkedQuestionMessage == null)
			{
				//Just show the answer, and then go back to the question. 
				Message newQuestionMessage = new Message();
				newQuestionMessage.questionmessage = theParentQuestionMessage;
				messages.Insert(0,newQuestionMessage);
				
				//Insert the final dialogue, in reverse order, to the messagequeue (because there may be messages in there). 
				for(int i = theRepliedQuestion.questionAnswers.Count-1; i>-1; i--)
				{
					Message aMessage = theRepliedQuestion.questionAnswers[i];
					messages.Insert(0, aMessage);
				}
				
				this.displayMessage();
			}
			else
			{
				Message newQuestionMessage = new Message();
				newQuestionMessage.questionmessage = theRepliedQuestion.linkedQuestionMessage;
				messages.Insert(0,newQuestionMessage);
				
				//Insert the final dialogue, in reverse order, to the messagequeue (because there may be messages in there). 
				for(int i = theRepliedQuestion.questionAnswers.Count-1; i>-1; i--)
				{
					Message aMessage = theRepliedQuestion.questionAnswers[i];
					messages.Insert(0, aMessage);
				}
				
				this.displayMessage();
			}
		}
		
		
	}

    public bool isMessageDisplaying()
    {
        return messageIsDisplaying;
    }	
	
}