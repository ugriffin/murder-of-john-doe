﻿using UnityEngine;
using System.Collections;

public struct Player {
	public float x;
	public float y;
}

public class AlwaysFacePlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//Player p1;
		//Player p2;

		GameObject thePlayer = GameState.instance.mainCharacter; //GameObject.Find ("FPSController");

		if (thePlayer != null)
		{
			//Vector3 point = thePlayer.transform.position;
			//point.x = 90;
			transform.LookAt(thePlayer.transform);

			//Quaternion newPosition = new Quaternion(90,this.gameObject.transform.rotation.y,this.gameObject.transform.rotation.z);
			this.gameObject.transform.eulerAngles = new Vector3(90,this.gameObject.transform.eulerAngles.y,this.gameObject.transform.eulerAngles.z);

			//this.gameObject.transform.eulerAngles.x = 90;
		}

	}
}
