﻿using UnityEngine;
using System.Collections;

public class LadderScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		GameState.instance.isTouchingLadder = true;
	}

	void OnTriggerExit(Collider other)
	{
		GameState.instance.isTouchingLadder = false;
	}
}
