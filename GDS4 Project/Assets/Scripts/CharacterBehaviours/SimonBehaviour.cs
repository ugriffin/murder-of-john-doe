﻿using UnityEngine;
using System.Collections;

public class SimonBehaviour : MonoBehaviour
{
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			switch(GameState.instance.theGameState)
			{
				case 2:
				{
					MessageDelegate.instance.showMessage("Simon: Hello there m-miss… Sorry uh what? I thought I was saying something c-crazy here.");
					MessageDelegate.instance.showMessage("You: Huh? Uh, no! Certainly not! Crazy isn’t something people… *Ahem* Can I ask you about something mister?");
				
					QuestionMessage aNewQuestionMessage = new QuestionMessage();
					aNewQuestionMessage.questionMessage = "Simon: Don’t ask me if I’m crazy or I-I’ll uh… Wha- Huh? Who are ya, n-need something?";
					
					Question aQuestion = new Question ();
					aQuestion.question = "1. Can you tell me anything at all about the murder?";
					aQuestion.addAnswer ("Simon: NO! ");
					aQuestion.addAnswer ("You: Whoa!!");
					aQuestion.addAnswer ("Simon: There's no murder here, no one's dead, you hear me!?");
					aQuestion.addAnswer ("You: Sure! Sure! (He’s scary! Could be dangerous too!)");
					aQuestion.callBack = new Callback(this.DangerSelected);
					
					aNewQuestionMessage.questions.Add (aQuestion);
					
					Question eQuestion = new Question ();
					eQuestion.question = "2. What are you doing around here? ";
					eQuestion.addAnswer ("Simon:.....");
					eQuestion.addAnswer ("You: Hello? Anyone home?");
					eQuestion.addAnswer ("Simon:.....");
					eQuestion.addAnswer ("Simon:.....");
					aNewQuestionMessage.addQuestion (eQuestion);
					
					Question bQuestion = new Question ();
					bQuestion.question = "3. Feeling alright?";
					bQuestion.addAnswer ("Simon: I'm feeling real good... Reeaal calm too. Because I’m hanging on real fine too…");
					bQuestion.addAnswer ("You: Go find help, otherwise-");
					bQuestion.addAnswer ("Simon: Never. They help only themselves.");
					aNewQuestionMessage.addQuestion (bQuestion);
					
					Question zQuestion = new Question ();
					zQuestion.question = "Goodbye.";
					zQuestion.addAnswer ("Simon: Hurr? Fuck me...");
					zQuestion.endsDialogue = true;
					aNewQuestionMessage.addQuestion (zQuestion);
					
					MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage,MessageEffect.Huh2);
					break;
				}
				default:
				{
					MessageDelegate.instance.showMessage("Simon: Hello there m-miss… Sorry uh what? I thought I was saying something c-crazy here.");
					break;
				}
			}
		}
	}
	
	void DangerSelected()
	{
		GameState.instance.addDeathState(1);
	}
}
