﻿using UnityEngine;
using System.Collections;

public class shaunBehaviour : MonoBehaviour
{
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
				if(GameState.instance.theGameState <= 5)
				{
					MessageDelegate.instance.showMessage("Shaun: Are you quite sure it’s a good time to be visiting town miss?");
					MessageDelegate.instance.showMessage("You: There’s a kid nearby, are you going to tell her off as well?");
					MessageDelegate.instance.showMessage("Shaun: She insisted to be here, but it’s her bad choice. And you should avoid this town.");
					MessageDelegate.instance.showMessage("You: If it’s so bad, why aren’t you leaving?");
					MessageDelegate.instance.showMessage("Shaun: I can’t. Not now when I’m not finish with business of my own.");
					MessageDelegate.instance.showMessage("You: You’re a funny man. Stand around and see if you can solve anything.");
					MessageDelegate.instance.showMessage("You: But you have time now don’t you? I’ll leave after you answer some questions.");
					
					QuestionMessage aNewQuestionMessage = new QuestionMessage();
					aNewQuestionMessage.questionMessage = "Shaun: So be it.";
					
					Question aQuestion = new Question ();
					aQuestion.question = "1. Can you tell me anything at all about the murder?";
					aQuestion.addAnswer ("Shaun: I did. A drunk, loud and obnoxious man coming out from a restaurant found himself a refuge in darkness and it betrayed him. \nThe man should have stayed in the light.");
					aQuestion.addAnswer ("You: Does it mean that you’re aware of a killer before he was…?");
					aQuestion.addAnswer ("Shaun: This town knows it, just not willing to acknowledge it. They will say that the killings are accidental because this town is \ninnocent. ");
					aQuestion.addAnswer ("You: Y-You seem pretty informed…");
					aQuestion.addAnswer ("Shaun: When you walk around the streets and hear the things locals say earlier, you’ll understand.");
					aQuestion.addAnswer ("You: Then I’d like to know what they’re hiding. They can’t hope to keep ever silent.");
					aQuestion.addAnswer ("Shaun: I know your type. If you’re looking to find the killer, then you’re bound to find clues. They will always be clues somewhere. \nUp or down.");
					aQuestion.addAnswer ("You: Are you, helping me?");
					aQuestion.addAnswer ("Shaun: Don’t look so suspicious woman. You wrote your own death sentence, but I’m willing to help. Just don’t except me to move just yet.");
					aQuestion.addAnswer ("You: I prefer that you stay here where I can see you. Oh and thanks for the info.");
					aNewQuestionMessage.questions.Add (aQuestion);
					
					Question eQuestion = new Question ();
					eQuestion.question = "2. How do I get closer to the truth?";
					eQuestion.addAnswer ("Shaun: The people in town will speak to you but will attempt to hide the truth. Their reasons for it are sinister. See that \nShanghai Lo Ching Hing restaurant to my left with that big red sign? You will see men who are loyal to their brothers and they share the same sins. \nTheir words will kill, and they will keep suffering in a deep and dark psychological abyss because they embrace the sins that they committed. ");
					eQuestion.addAnswer ("Shaun: They are willing to rid those asking odd questions. If you think it’s not a good idea to ask someone about something. Don’t do it. ");
					eQuestion.addAnswer ("Shaun: This applies the same when confronting someone about something too. But if you’re willing to risk that for a shorter \nroute to an answer, well… That’s your decision. When you’re done go to a police officer and give them an answer. Make every move count.");
					eQuestion.addAnswer ("You: That sounds wise… I’ll remember that.");
					aNewQuestionMessage.addQuestion (eQuestion);
					
					Question bQuestion = new Question ();
					bQuestion.question = "3. Any further advice? ";
					bQuestion.addAnswer ("Shaun: What you're doing is criminal, but necessary right now. Don't let the cop get you. Anyway...");
					bQuestion.addAnswer ("Shaun: When you get to where Shanghai Lo Ching Hing restaurant is, you know with the big red sign, look around for a back alley \nand tread carefully. You are bound to find something, a clue, your ace in a hole.");
					bQuestion.addAnswer ("You: Find clues in back alleys huh? No need to wish me luck, we journalist will never keep our nose out of people’s business, our lives are \nmeant to be a risky one.");
					bQuestion.callBack = new Callback(this.canTalkToHayman);
					aNewQuestionMessage.addQuestion (bQuestion);
					
					Question zQuestion = new Question ();
					zQuestion.question = "Goodbye.";
					zQuestion.addAnswer ("Shaun: Good luck.");
					zQuestion.endsDialogue = true;
					aNewQuestionMessage.addQuestion (zQuestion);
					
					MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage);
					
					if(GameState.instance.theGameState == 4)
					{
						GameState.instance.theGameState = 5;
					}
				}
				else
				{
					MessageDelegate.instance.showMessage("Shaun: Life sure is strange, isn't it...");
					MessageDelegate.instance.showMessage("You: It sure is.");
				}
			}
		
	}
	
	void canTalkToHayman()
	{
		GameState.instance.canTalkToHayman = true;
	}
}
