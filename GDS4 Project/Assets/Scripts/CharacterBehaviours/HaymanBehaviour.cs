﻿using UnityEngine;
using System.Collections;

public class HaymanBehaviour : MonoBehaviour
{

	public bool isGameOver = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
		if(isGameOver == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			Application.LoadLevel("GameOverScene");
		}
    }

    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
        {
			if(GameState.instance.canTalkToHayman == false)
        	{
            	MessageDelegate.instance.showMessage("Hayman: Miss, step away, we cannot allow journalists here right now.");
            	MessageDelegate.instance.showMessage("You: Guess you boys don’t know who the murderer is yet huh? (Looks like I have to find a way to even see the body)");
            	MessageDelegate.instance.showMessage("Hayman: I won’t say a damn word miss, and no more talking.");
            }
            else
            {
				MessageDelegate.instance.showMessage("Hayman: Miss? You better not be here to berate me, I’m not in the mood.",MessageEffect.Hmm);
				MessageDelegate.instance.showMessage("You: I think I may have found the killer, Hayman.");
				
				QuestionMessage aNewQuestionMessage = new QuestionMessage();
				aNewQuestionMessage.questionMessage = "(Whose name should I report to him?)";
				
				//Question derpQuestion = new Question();
				//derpQuestion.question = "James";
				//derpQuestion.addAnswer("You: Ehh, they look awful, but I doubt he had anything to do with the killing.");
				//aNewQuestionMessage.addQuestion(derpQuestion);
				
				Question aQuestion = new Question();
				aQuestion.question = "Logan";
				aQuestion.addAnswer("You: No doubt about it, he knows too much about the incident where the man lost his family.\nIt must be convenient for him to push the blame indirectly towards others.\nHis little act and whatever’s inside the suitcase is no doubt an effort to sway me off the real killer.\n\nHim.");
				aQuestion.addAnswer("The killing in the town where the lawyer was hanged never stopped.\nNobody expected to be safe anymore. Difficult to escape, the inhabitants remained as long as they kept mum.\nThe man in custody never spoken who the killer is, loyal to the end. Brothers forever.\nAnd you? Your story proved to be nothing more than a Sunday morning read, where adults pick up to read and toss it aside, ready to get over with their daily lives…");
				aNewQuestionMessage.addQuestion(aQuestion);
				
				Question bQuestion = new Question();
				bQuestion.question = "Simon";
				bQuestion.addAnswer ("You: That’s right. The one who fits the description. Crazy, unpredictable.");
				bQuestion.addAnswer ("You: The one from the gang in which was part of a gang years ago.\nHe chose to kill the victim, a lawyer due to his actions leading to the release of a killer.");
				bQuestion.addAnswer ("You: The killer who slaughtered an entire family, leaving one behind.");
				bQuestion.addAnswer("You: Simon and his band of brothers, despite abandoned by their leader, never stopped looking for the killer.\nBut our main focus is the lawyer’s killer, there’s apparently a similar incident like tonight.\nStill, I believe Simon is our killer tonight. I just have to find the previous murderer next time.");
				bQuestion.addAnswer("News spread around town. And also everywhere in the city outside.\nThe news of three tragic members of a gang that was broken years ago caught the eye of the public.\nThe capture of Simon, aka Madder led to series of arrests. Starting with Harry and Logan.\nThe two submitted themselves over to the authorities, the two remained loyal to their bound brother, sharing their sins together.\nThis is the story that was publicized; the person responsible for the happy ending was praised for a well done in her performance.\nWith a steady career and undying support from others, she can only look forward to what’s next for her.\nA murder mystery? Maybe, no one knows.");
				bQuestion.callBack = new Callback(this.gameWin);
				aNewQuestionMessage.addQuestion(bQuestion);
				
				Question xQuestion = new Question();
				xQuestion.question = "Don";
				xQuestion.addAnswer ("You: Ehh, they look awful, but I doubt he had anything to do with the killing.");
				aNewQuestionMessage.addQuestion(xQuestion);
				
				Question yQuestion = new Question ();
				yQuestion.question = "Eddie";
				yQuestion.addAnswer ("You: Ehh, they look awful, but I doubt he had anything to do with the killing.");
				aNewQuestionMessage.addQuestion (yQuestion);
				
				Question zQuestion = new Question ();
				zQuestion.question = "Goodbye";
				zQuestion.addAnswer ("Hayman: I wish you'd stop wasting my time. We have actual work to do here.");
				zQuestion.endsDialogue = true;
				aNewQuestionMessage.addQuestion (zQuestion);
				
				MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage);
            }
        }
    }
    
    void gameWin()
    {
    	isGameOver = true;
    }
    
    void gameOver()
    {
		isGameOver = true;
   	}
}
