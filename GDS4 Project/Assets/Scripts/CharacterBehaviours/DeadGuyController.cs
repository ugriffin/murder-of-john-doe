﻿using UnityEngine;
using System.Collections;

public class DeadGuyController : TargetableObject {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown() {
	}

	public override void targetObjectClicked()
	{
		if(GameState.instance.theGameState == 0)
		{
			if (MessageDelegate.instance.isMessageDisplaying() == false) {
				MessageDelegate.instance.showMessage("You have taken a photograph of the dead body.");
				GameState.instance.theGameState = 1;
			}
		}

	}
}
