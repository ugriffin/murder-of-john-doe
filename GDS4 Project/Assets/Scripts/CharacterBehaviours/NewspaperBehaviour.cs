﻿using UnityEngine;
using System.Collections;

public class NewspaperBehaviour : TargetableObject {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	public override void targetObjectClicked()
	{
		if(GameState.instance.theGameState == 5)
		{
			if (MessageDelegate.instance.isMessageDisplaying() == false) {
				MessageDelegate.instance.showMessage("A newspaper lays on the ground.");
				MessageDelegate.instance.showMessage("Pieces of a newspaper showing the deaths of children in the family, husband left with nobody after his wife committed suicide. ");
				MessageDelegate.instance.showMessage("Husband is apparently part of a gang that has been broken up after the incident.");
				MessageDelegate.instance.showMessage("Of course, it’s torn so there’s no way in hell to know what they look like.");
				
				GameState.instance.foundNewsPaper = true;
			}
		}
		
	}
	
}
