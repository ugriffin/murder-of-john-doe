﻿/*James Positions
Game State 0:

*/
using UnityEngine;
using System.Collections;

public class JamesBehaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		switch(GameState.instance.theGameState)
		{
			case 0:
			{
				gameObject.transform.position = new Vector3(33.36f,5.0f,101.098f);
				break;
			}
			case 1:
			{
				gameObject.transform.position = new Vector3(52.29f,10.63f,88.86f);
				break;
			}
			default:
			{
				break;
			}
			
		}
		
	}

    void OnTriggerStay (Collider other) {
		if(Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
        {
        
        	switch(GameState.instance.theGameState)
        	{
        		case 0:
        		{
					MessageDelegate.instance.showMessage("James: Hello, uh, Miss! Doubtful that you have heard but a murder has taken place. Right over there in that alleyway.");
					MessageDelegate.instance.showMessage("You: Apparently this is the second time; anyhow please excuse me.");
        			break;
        		}
        		case 1:
				{
					MessageDelegate.instance.showMessage("James: Don’t worry about me miss, I will not tell on you.");
					MessageDelegate.instance.showMessage("You: Good. I rather not have somebody ruining my opportunity, I want answers and the police aren’t helping at all.");
					MessageDelegate.instance.showMessage("James: You are really blunt aren’t you? Cops don’t have answers to anything either, locals aren’t too cooperative.",MessageEffect.Huh1);
					MessageDelegate.instance.showMessage("James: They probably don’t like cops, you can try and nose around if you want, ask questions and whatnot.");
					MessageDelegate.instance.showMessage("You: Huh, then you wouldn’t mind me asking you questions.");
					
					QuestionMessage aNewQuestionMessage = new QuestionMessage();
					aNewQuestionMessage.questionMessage = "James: No, not at all, not at all...";
					
					Question aQuestion = new Question ();
					aQuestion.question = "1. Can you tell me anything at all about the murder?";
					aQuestion.addAnswer ("James:  Sure thing, the man was looking a bit tipsy, was heading straight from the restaurant and into the alleyway.");
					aQuestion.addAnswer ("You: You were just standing around and saw him?");
					aQuestion.addAnswer ("James: That’s right, saw him head first into the alleyway.?");
					aNewQuestionMessage.questions.Add (aQuestion);
					
					Question eQuestion = new Question ();
					eQuestion.question = "2. What are you doing around here?";
					eQuestion.addAnswer ("James: What am I doing? Wandering around… I guess nothing really.");
					eQuestion.addAnswer ("You: Doing absolutely nothing at all..?");
					eQuestion.addAnswer ("James: Dunno, came from somewhere… and ended up here.");
					eQuestion.addAnswer ("You: (Waste of time, who else is useful here..?)");
					aNewQuestionMessage.addQuestion (eQuestion);
					
					Question zQuestion = new Question ();
					zQuestion.question = "Goodbye.";
					zQuestion.addAnswer ("James: Good luck.");
					zQuestion.endsDialogue = true;
					aNewQuestionMessage.addQuestion (zQuestion);
					
					MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage);
					
					MessageDelegate.instance.showMessage("You've just finished questioning a person.");
					MessageDelegate.instance.showMessage("Pay close attention to the answers that people give you, they might just be useful.");
					MessageDelegate.instance.showMessage("The trick to beating this game is to question as many people as possible.");
					MessageDelegate.instance.showMessage("They might talk about things that point to new clues.");
					MessageDelegate.instance.showMessage("Remember to always talk to people and photograph potential clues and things.");
					MessageDelegate.instance.showMessage("Good luck!");
					
					GameState.instance.theGameState = 2;
				
				
        			break;
        		}
        		
        		default:
        		{
					MessageDelegate.instance.showMessage("James: Hello, Miss! How goes your investigation?");
					MessageDelegate.instance.showMessage("You: It's moving slowly, James. Slowly.");
					MessageDelegate.instance.showMessage("James: Sorry to hear that, Miss. You have a good day now.");
        			break;
        		}
            } 
			
        }
	}
}
