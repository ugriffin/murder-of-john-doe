﻿using UnityEngine;
using System.Collections;

public class WomanBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			MessageDelegate.instance.showMessage("Vicki: Oh dear I'm glad to see yet another commoner, sorry honey, Vicki Garnett's not accepting autographs.");
			MessageDelegate.instance.showMessage("You: Yet you're here, I know you just came here to be like us 'commoners'. Why else would you come here?");
			MessageDelegate.instance.showMessage("Vicki: I am living, and breathing proof of superiority. Don't spit on it, not on the truth.");
			MessageDelegate.instance.showMessage("You: It’s funny how you see this as amusing, but you’re wasting your time.");
			MessageDelegate.instance.showMessage("Vicki: How disgusting. Were you made fun of the way you speak before? I saw you feeling irritated just now.");
			MessageDelegate.instance.showMessage("Vicki: No worries my dear, I am willing to turn you from nothing, and into somebody.");
			MessageDelegate.instance.showMessage("You: I was thinking about the same idea. (Sorry honey, your name will appear on my journal,\nnow people all around can hear of this particular attraction of ugly.)");
		}
	}
}