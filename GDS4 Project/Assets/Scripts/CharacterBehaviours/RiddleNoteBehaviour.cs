﻿using UnityEngine;
using System.Collections;

public class RiddleNoteBehaviour : TargetableObject {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	public override void targetObjectClicked()
	{
		if (MessageDelegate.instance.isMessageDisplaying() == false && GameState.instance.theGameState == 3)
		{
			MessageDelegate.instance.showMessage("A note with a riddle. This handwriting is unique.");
			MessageDelegate.instance.showMessage("It look like its hastily written but you can get a sense of someone with a broken mind had written this:");
			MessageDelegate.instance.showMessage("“My quests lies within where no one can see me on ground and the past”");
			GameState.instance.theGameState = 4;
		}
	}
}
