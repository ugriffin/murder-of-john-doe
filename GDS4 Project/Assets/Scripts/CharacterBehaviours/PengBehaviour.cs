﻿using UnityEngine;
using System.Collections;

public class PengBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			MessageDelegate.instance.showMessage("You: Evening. What's a young lady like you doing here out so late?");
			MessageDelegate.instance.showMessage("Peng: Oh hello. I was... I am just waiting for somebody...");
			MessageDelegate.instance.showMessage("You: Somebody? You mean your friends?");
			MessageDelegate.instance.showMessage("Peng: Y-Yes! My friends! From school. We are suppose to meet up right here.");
			MessageDelegate.instance.showMessage("You: (Sure you are... She most likely ran away from home.)");
			MessageDelegate.instance.showMessage("Peng: My name is Peng. Um, you're not going to tell me off are you?");
			MessageDelegate.instance.showMessage("You: No. But you should just head inside the karoake lounge.");
			MessageDelegate.instance.showMessage("Peng: It smells of cigarette, I hate it.");
			MessageDelegate.instance.showMessage("You: It should be warmer inside kid, you don’t wanna get a cold right?");
			MessageDelegate.instance.showMessage("Peng: No. I absolutely hate it. Oh no, I smell a bit of it right now. I-I might move.");
			MessageDelegate.instance.showMessage("You: I can’t smell a thing. Oh and hey, stay right here where people can see you. Trust me.");
			MessageDelegate.instance.showMessage("Peng: … I’ll try.");
		}
	}
}