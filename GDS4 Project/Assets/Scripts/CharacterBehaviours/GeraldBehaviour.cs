﻿using UnityEngine;
using System.Collections;

public class GeraldBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			if(GameState.instance.theGameState == 5)
			{
				MessageDelegate.instance.showMessage("Gerald: You.");
				MessageDelegate.instance.showMessage("You: Me? Well, I’m just looking into the murder so I have something to write about. Nicely of course.");
				MessageDelegate.instance.showMessage("Gerald: What is your name?");
				MessageDelegate.instance.showMessage("You: My name’s not important, but you can tell me yours if you like.");
				MessageDelegate.instance.showMessage("Gerald: Gerald. I’m glad you’re not a phantom. This people around us, fuckin’ creepy. Talking all crazy like that I could puke.");
				MessageDelegate.instance.showMessage("Gerald: One more word from them and I might hit them. I don't play around, no sir.");
				MessageDelegate.instance.showMessage("You: Before you do that, let’s talk a little. ");
				QuestionMessage aNewQuestionMessage = new QuestionMessage();
				aNewQuestionMessage.questionMessage = "Gerald: Out with it.";
				
				Question aQuestion = new Question ();
				aQuestion.question = "1. Can you tell me anything at all about the murder?";
				aQuestion.addAnswer ("Gerald: Yeah I do, the lawyer guy. I was there, eating at my usual place. He drank so much liquor he spilt so many secrets ‘bout his work!\nI didn’t tell anyone ‘bout this, but I definitely saw him hanged.");
				aQuestion.addAnswer ("You: You witnessed his death? You… talked to the police?");
				aQuestion.addAnswer ("Gerald: Heck no man, I mean I wanna live too ya know? I mean this killer just came outta nowhere and… ya know?");
				aQuestion.addAnswer ("You: Do you mind telling me what he looks like?");
				aQuestion.addAnswer ("Gerald: Well, it looked like he wore a hat but everything else are too dark to be seen. That’s all lady.");
				aNewQuestionMessage.questions.Add (aQuestion);
				
				Question eQuestion = new Question ();
				eQuestion.question = "2. You see anything interesting besides the phantoms interesting tonight?";
				eQuestion.addAnswer ("Gerald: Well, I see some odd things around the back alleys in town. And I don’t mean rubbish and stuff. It’s probably weirdos leaving stuff there, \nlike somebody’s coming back for them.");
				eQuestion.addAnswer ("You: Hello? Anyone home?");
				aNewQuestionMessage.addQuestion (eQuestion);
				
				if(GameState.instance.foundNewsPaper)
				{
					Question bQuestion = new Question ();
					bQuestion.question = "3. You’re involved in some gang before right?";
					bQuestion.addAnswer ("Gerald: Boy, do I look like some gang member to you? C’mon, don’t judge a man by his clothing.",MessageEffect.Hmm);
					bQuestion.addAnswer ("You: It’s just a question, but have you seen this newspaper? (I showed him the one I found.)");
					bQuestion.addAnswer ("Gerald: That’s news that I remember all the time. Boy, hard times when losing your whole family.");
					bQuestion.addAnswer ("You: True. And this gang leader disbanded his group. Leaving behind his people.");
					bQuestion.addAnswer ("Gerald: I guess. But apparently they do not bother looking for him. Anyway, what do I know? They could very well still be around, somewhere… \nLike a phantom.");
					aNewQuestionMessage.addQuestion (bQuestion);
				}
				
				Question zQuestion = new Question ();
				zQuestion.question = "Goodbye.";
				zQuestion.addAnswer ("Gerald: Be careful lady, ya hear?");
				zQuestion.endsDialogue = true;
				aNewQuestionMessage.addQuestion (zQuestion);
				
				MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage);
			}
			else
			{
				MessageDelegate.instance.showMessage("Gerald: What is your name?");
				MessageDelegate.instance.showMessage("You: My name’s not important, but you can tell me yours if you like.");
				MessageDelegate.instance.showMessage("Gerald: Gerald. I’m glad you’re not a phantom. This people around us, fuckin’ creepy. Talking all crazy like that I could puke.");
				MessageDelegate.instance.showMessage("Gerald: One more word from them and I might hit them. I don't play around, no sir.");
			}
			
			
		}
	}
}
