﻿using UnityEngine;
using System.Collections;

public class HarryBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			if(GameState.instance.theGameState == 5)
			{
				MessageDelegate.instance.showMessage("Harry: *Grunts*");
				MessageDelegate.instance.showMessage("You: Hello there, can I have a talk with you?");
				MessageDelegate.instance.showMessage("Harry: (He laughs softly, his eyes piercing directly at you.)");
				MessageDelegate.instance.showMessage("You: (How do I approach this, should I even talk to him in the first place..?)");
				QuestionMessage aNewQuestionMessage = new QuestionMessage();
				aNewQuestionMessage.questionMessage = "Harry:....";
				
				Question aQuestion = new Question ();
				aQuestion.question = "1. Can you tell me anything at all about the murder?";
				aQuestion.addAnswer ("Harry: Heh heh heh. Why yes, it’s recent news all over town. Everybody knows. You know. I know. ");
				aQuestion.addAnswer ("You: It’s horrible, I want to help this town capture the murderer. Can you help?");
				aQuestion.addAnswer ("Harry: With that camera of yours? You’re a journalist, you want to spread the disease over town. Spoiling our name? This town doesn’t need you. ");
				aQuestion.addAnswer ("You: Hey, hey, more murders in town’s probably a bad idea right?");
				aQuestion.callBack = new Callback(this.addDanger);
				aNewQuestionMessage.questions.Add (aQuestion);
				
				if(GameState.instance.foundNewsPaper)
				{
					Question eQuestion = new Question ();
					eQuestion.callBack = new Callback(this.addDanger);
					eQuestion.question = "2. You’re involved in some gang before right? ";
					eQuestion.addAnswer ("Harry: What if I was? What are you going to do about it?",MessageEffect.Huh2);
					eQuestion.addAnswer ("You: A few years ago, some leader of a gang lost his family; you’re part of it and wanted to find revenge right?");
					eQuestion.addAnswer ("Harry: That’s right, the three of us will find the one who did, he’s here and we are going to get him. Me, Logan the Slayer and Madder!\nPeople call him crazy, calls him mad to be place into insane asylums, but they don’t know anything about him. \nNow stop talking. You are at fault. You will pay for this.");
					aNewQuestionMessage.addQuestion (eQuestion);
				}
				
				Question bQuestion = new Question ();
				bQuestion.question = "3. Feeling alright?";
				bQuestion.addAnswer ("Harry: I feel fine. Don’t need to ask , I’ve no need for your concern.");
				aNewQuestionMessage.addQuestion (bQuestion);
				
				Question zQuestion = new Question ();
				zQuestion.question = "Goodbye.";
				zQuestion.addAnswer ("Harry: Watch your ass lady.");
				zQuestion.endsDialogue = true;
				aNewQuestionMessage.addQuestion (zQuestion);
				
				MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage);
			}
			else
			{
				MessageDelegate.instance.showMessage("Harry: *Grunts*");
				MessageDelegate.instance.showMessage("You: Hello there, can I have a talk with you?");
				MessageDelegate.instance.showMessage("Harry: (He laughs softly, his eyes piercing directly at you.)");
			}
		}
	}
	
	void addDanger()
	{
		GameState.instance.addDeathState(1);
	}
}
