﻿using UnityEngine;
using System.Collections;

public class DonBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerStay (Collider other) {
		if(Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			switch(GameState.instance.theGameState)
			{
				case 2:
				{
					MessageDelegate.instance.showMessage("Don: Looking to get yourself choked too?");
					MessageDelegate.instance.showMessage("You: Not my first preference of dying, and you?");
					MessageDelegate.instance.showMessage("Don: (snicker) Maybe it is! HEH! Bet you want me dead too!");
					MessageDelegate.instance.showMessage("You: You'd be a great help, but first, let’s discuss about the person of the 12th Tarot card.");
		
					QuestionMessage aNewQuestionMessage = new QuestionMessage();
					aNewQuestionMessage.questionMessage = "Don: The what now...?";
					
					Question aQuestion = new Question ();
					aQuestion.question = "1. Can you tell me anything at all about the murder?";
					aQuestion.addAnswer ("Don:  Ha ha haa! A man ~ had too much to drink~ Says he found a way to forget! Said its time to let go~ Boy, did he ever. \nFound comfort in hanging, poor ol’ boy!");
					aQuestion.addAnswer ("You: Did you really think he committed suicide?");
					aQuestion.addAnswer ("Don: Why would somebody target a poor sod like that? Or rather care?");
					aQuestion.addAnswer ("You: There’s surely more to it.");
					aQuestion.addAnswer ("Don: Like I even care...");
					aNewQuestionMessage.questions.Add (aQuestion);
					
					Question eQuestion = new Question ();
					eQuestion.question = "2. What are you doing around here? ";
					eQuestion.addAnswer ("Don: Graffiti art here is strange, it doesn't make sense at all.");
					eQuestion.addAnswer ("You: They never will.");
					eQuestion.addAnswer ("Don: You’ll never understand art, but the ones here are wack! Like the one behind the \ntwin buildings at the front with some Korean shop.");
					eQuestion.addAnswer ("You: (If he’s confused, then finding it should prove interesting.)");
					eQuestion.callBack = new Callback(this.rightQuestionAnsweredCallback);
					aNewQuestionMessage.addQuestion (eQuestion);
					
					Question bQuestion = new Question ();
					bQuestion.question = "3. Why did you think I’d want you dead?";
					bQuestion.addAnswer ("Don: Some little punk said I started too much trouble and soon I’m going to get it. (snicker) Yeah right!");
					bQuestion.addAnswer ("You: What about the victim? Did he cause trouble too?");
					bQuestion.addAnswer ("Don: Never seen his ugly mug before.");
					aNewQuestionMessage.addQuestion (bQuestion);
					
					Question cQuestion = new Question ();
					cQuestion.question = "4. Interested in luring the killer out?";
					cQuestion.addAnswer ("Don: Haha, I think killers would be more interested in you little miss.");
					cQuestion.addAnswer ("You: A good looking gentleman like you will surely lure him out easily.");
					cQuestion.addAnswer ("Don: Funny girl, even I wouldn’t be fooled into this.");
					cQuestion.addAnswer ("You: A shame, I would have caught the killer on camera and cops will finally have something to do.");
					cQuestion.addAnswer ("Don: What makes you think killers would wander on ground and not on top?");
					aNewQuestionMessage.addQuestion (cQuestion);
					
					Question zQuestion = new Question ();
					zQuestion.question = "Goodbye.";
					zQuestion.addAnswer ("Don: Whatever...");
					zQuestion.endsDialogue = true;
					aNewQuestionMessage.addQuestion (zQuestion);
		
					MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage,MessageEffect.Hmm);
					break;
				}
				
				default:
				{
					MessageDelegate.instance.showMessage("Don: Looking to get yourself choked too?");
					MessageDelegate.instance.showMessage("You: Not my first preference of dying, and you?");
					MessageDelegate.instance.showMessage("Don: (snicker) Maybe it is! HEH! Bet you want me dead too!");
					break;
				}
			}
			

		}
	}
	
	void rightQuestionAnsweredCallback()
	{
		MessageDelegate.instance.showMessage("You: He mentioned something about a graffiti somewhere...?");
		MessageDelegate.instance.showMessage("You: Maybe that has some clues. I should check it out.");
	}
}
