﻿using UnityEngine;
using System.Collections;

public class OldManBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{

	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			MessageDelegate.instance.showMessage("Han: Sorry dear, shop's closed. Been a murder, you heard?");
			MessageDelegate.instance.showMessage("You: Yeah... I heard about it, thinking about writing about it for a job. But I can't just find answers by just looking...");
			MessageDelegate.instance.showMessage("Han: I'm afraid I'm the wrong person to interview, there are others who can give you answers. Maybe...");
			MessageDelegate.instance.showMessage("You: You're a busy man, so it's no surprise that you have not heard a thing other than the murder that took place.");
			MessageDelegate.instance.showMessage("Han: Busy's right. That lady over there is amusing, using her ring as payment for the meal. Heheh, idiot. Should sell this off\nand thank her later.");
			MessageDelegate.instance.showMessage("You: She sure look nasty. That the reason?");
			MessageDelegate.instance.showMessage("Han: Yeah, it's a funny case of idiocy. She ain't never getting her ring back. Not even if she brings her twenty boyfriends from\nthe uptown districts.");
			MessageDelegate.instance.showMessage("You: Well, now it cannot be helped.");
		}
	}
}