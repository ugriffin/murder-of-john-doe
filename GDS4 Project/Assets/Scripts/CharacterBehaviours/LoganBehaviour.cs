﻿using UnityEngine;
using System.Collections;

public class LoganBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			if(GameState.instance.theGameState == 5)
			{
				MessageDelegate.instance.showMessage("Logan: Why hello there, how do you fare tonight?");
				MessageDelegate.instance.showMessage("You: I’m just fine, thank you. You a local here?");
				MessageDelegate.instance.showMessage("Logan: Afraid not my lady, but I was here for a while in town. My, my, what a quaint little town.");
				MessageDelegate.instance.showMessage("You: Well, then. I have something to ask of you. So don’t go anywhere alright?");
	
				QuestionMessage aNewQuestionMessage = new QuestionMessage();
				aNewQuestionMessage.questionMessage = "Logan: Whatever pleases the lady.";
				
				Question aQuestion = new Question ();
				aQuestion.callBack = new Callback(this.addDanger);
				aQuestion.question = "1. Can you tell me anything at all about the murder?";
				aQuestion.addAnswer ("Logan: Why a poor lad let his “lawyering” go too far and he ended up in the wrong side of town.");
				aQuestion.addAnswer ("You: How did you know he’s a lawyer?");
				aQuestion.addAnswer ("Logan: W-Wha?",MessageEffect.Huh1);
				aQuestion.addAnswer ("Logan: Well my dear, everybody knows by now. People here knows that.");
				aQuestion.addAnswer ("You: Okay... What about the rest in town, they seem clueless.");
				aQuestion.addAnswer ("Logan: Well, they probably aren’t uh told about it by the police.");
				aQuestion.addAnswer ("You: The police, well, he doesn’t know anything. Only you right now.");
				aQuestion.addAnswer ("Logan: Hahaha oh silly me.");
				aNewQuestionMessage.questions.Add (aQuestion);
				
				Question eQuestion = new Question ();
				aQuestion.callBack = new Callback(this.addDanger);
				eQuestion.question = "2. I’m sure you’ve seen something interesting, a unique man like yourself.";
				eQuestion.addAnswer ("Logan: Oh ho ho, why would you ever say so? I only visit town because it is said to be popular among the youthful population.",MessageEffect.Huh2);
				eQuestion.addAnswer ("You: But the way you are dressed. It’s about how you dressed as well, and with a suitcase and all. You here on business then?");
				eQuestion.addAnswer ("Logan: Forgive me. But I do feel uncomfortable having a stranger like you peddle into my business. No offence, as lovely as you are my lady, \nI mind my own and you mind yours.");
				eQuestion.addAnswer ("You: Alright then. (I think he kind of stuttered a little. I know he’s hiding something. But I’ll get in trouble pestering him about this).");
				aNewQuestionMessage.addQuestion (eQuestion);
				
				if(GameState.instance.foundNewsPaper)
				{
					Question bQuestion = new Question ();
					bQuestion.callBack = new Callback(this.addExtraDanger);
					bQuestion.question = "3. You’re involved in some gang before right? ";
					bQuestion.addAnswer ("Logan: Excuse me. But how did you know about this? Who told you this? Harry?",MessageEffect.Huh1);
					bQuestion.addAnswer ("You: Yes, he did.");
					bQuestion.addAnswer ("Logan: Now you know. Do you really want to know my story? Your face says it does. I am part of a family. Not your average family mind you. \nBrothers bound by something great. I consider a brother’s blood related family as one of my own too you know. One day, when you heard that \nyour own brother’s family got killed in cold-blood...");
					bQuestion.addAnswer ("Logan: Naturally, we would chase and find the man who did it together. However, our leader, the man we call an older brother abandoned us. \nTelling us to forget about all this, and left us to nothing. But we’ll never forgive the man who did it. There’s Harry and me dear, \nand another one. He is also here. But not his usual self.");
					bQuestion.addAnswer ("Logan: So there. Are you happy? I told you everything I know. Perhaps, I know who the dreaded killer is, but the one he killed, deserved it.");
					aNewQuestionMessage.addQuestion (bQuestion);
				}
				
				Question zQuestion = new Question ();
				zQuestion.question = "Goodbye.";
				zQuestion.addAnswer ("Logan: Farewell.");
				zQuestion.endsDialogue = true;
				aNewQuestionMessage.addQuestion (zQuestion);
				
				MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage);
			}
			else
			{
				MessageDelegate.instance.showMessage("Logan: Why hello there, how do you fare tonight?");
				MessageDelegate.instance.showMessage("You: I’m just fine, thank you. You a local here?");
				MessageDelegate.instance.showMessage("Logan: Afraid not my lady, but I was here for a while in town. My, my, what a quaint little town.");
			}
			
		}
	}
	
	void addDanger()
	{
		GameState.instance.addDeathState(1);
	}
	
	void addExtraDanger()
	{
		GameState.instance.addDeathState(2);
	}
}
