﻿using UnityEngine;
using System.Collections;

public class EddieBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) == true && MessageDelegate.instance.isMessageDisplaying() == false)
		{
			if(GameState.instance.theGameState == 5)
			{
			MessageDelegate.instance.showMessage("Eddie: WHOA! Chicka! Don’t scare a homes like that!");
			MessageDelegate.instance.showMessage("You: S-Sorry, are you in some kind of gang? I mean, you’re dressed like that.");
			MessageDelegate.instance.showMessage("Eddie: Ha? You think so? Wh-what makes you say that? It’s not like I’m friends with the one who did that lawyer guy in!");
			MessageDelegate.instance.showMessage("You: The victim’s a lawyer?");
			QuestionMessage aNewQuestionMessage = new QuestionMessage();
			aNewQuestionMessage.questionMessage = "Eddie: Haha! Lawyer? Di-did I say that? Haha, forget about that please!";
			
			Question aQuestion = new Question ();
			aQuestion.question = "1. Can you tell me anything at all about the murder?";
			aQuestion.addAnswer ("Eddie: Well yeah that lawyer gu- I mean business man, yes, business man went into some alley to die right?");
			aQuestion.addAnswer ("You: He didn’t intend to- er nevermind. (Sheesh, trying so hard to hide the facts huh?)");
			aNewQuestionMessage.questions.Add (aQuestion);
			
			Question eQuestion = new Question ();
			eQuestion.callBack = new Callback(this.addDanger);
			eQuestion.question = "2. Surely you’re part of a gang, this town’s full of it. So it’s fine. ";
			eQuestion.addAnswer ("Eddie: No way! H-Hell no! Don’t say things like that homes!");
			eQuestion.addAnswer ("You: Well if you aren’t, I have to go look for them.");
			eQuestion.addAnswer ("Eddie: H-huh?! What for? You better stay away!",MessageEffect.Huh2);
			aNewQuestionMessage.addQuestion (eQuestion);
			
			if(GameState.instance.foundNewsPaper)
			{
				Question bQuestion = new Question ();
				bQuestion.question = "3. You’re involved in some gang before right? ";
				bQuestion.addAnswer ("Eddie: Those people? Nahh homes. Those people around us, one of them are probably.");
				bQuestion.addAnswer ("You: Know the story about those in involved in that incident?");
				bQuestion.addAnswer ("Eddie: Ehhhh, well I know the man lost his whole family and all. With his gang left, the only family left and all. Haaa, he disappeared too. ");
				bQuestion.addAnswer ("You: Have these guys looked for their leader? That’s why they’re here?");
				bQuestion.addAnswer ("Eddie: Doubt it homeeesss, I mean the leader left the gang with an important secret mission and stuff. Could be why they’re here, to fulfill that mission.");
				bQuestion.addAnswer ("You: I see… These gang members are here for something.");
				aNewQuestionMessage.addQuestion (bQuestion);
			}
			
			Question zQuestion = new Question ();
			zQuestion.question = "Goodbye.";
			zQuestion.addAnswer ("Eddie: Stay cool bro- I mean sister!");
			zQuestion.endsDialogue = true;
			aNewQuestionMessage.addQuestion (zQuestion);
			
			MessageDelegate.instance.showQuestionMessage(aNewQuestionMessage,MessageEffect.Huh1);
			}
			else
			{
			MessageDelegate.instance.showMessage("Eddie: WHOA! Chicka! Don’t scare a homes like that!");
			MessageDelegate.instance.showMessage("You: S-Sorry, are you in some kind of gang? I mean, you’re dressed like that.");
			}
			
			
		}


	}
	
	void addDanger()
	{
		GameState.instance.addDeathState(1);
	}
}
